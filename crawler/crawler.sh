#!/bin/sh

# Default value
: "${REFRESH_INTERVAL:=1m}"

MONGO_HOST="mongodb://mongowrite:$MONGO_PASSWORD@mongo/CN"

echo "Wait for MongoDB initialization"
until mongoexport --uri "$MONGO_HOST" -c Results > /dev/null
do
  sleep 1
done

while true
do
  echo "Fetch Jenkins data"
  if curl -m 10 -gsSf \
    "$JENKINS_HOST/api/json?tree=jobs[jobs[builds[url,description,timestamp,duration,actions[parameters[name,value],failCount,skipCount,totalCount]]]]" > \
    jenkins.json
  then
    echo "Process JSON"
    if jq \
      '.jobs[].jobs[]?.builds[] | {_id: .url[:-1],
       branch: .actions[].parameters[]? | select(.name == "BRANCH") | .value,
       hash: .actions[].parameters[]? | select(.name == "HASH") | .value[:8],
       profile: .actions[].parameters[]? | select(.name == "PROFILE") | .value | ascii_upcase,
       passed: .actions[] | select(.totalCount != null) | (.totalCount - .failCount - .skipCount),
       total: .actions[] | select(.totalCount != null) | .totalCount,
       note: (.description // ""),
       set: .actions[].parameters[]? | select(.name == "EXECUTION_TYPE") | .value,
       tags: ((.actions[].parameters[]? | select(.name == "TAGS") | .value) // ""),
       started: (.timestamp / 1000) | strftime("%Y-%m-%d %H:%M"),
       minutes: (.duration / 60000) | floor}' \
      jenkins.json > all.json
    then
      jq -c 'select((._id | contains("Parallel")) or (.set != "parallel")) |
        if .tags == "sanity_zts" then
          .set = "sanity"
        elif .tags == "smoke_zts" then
          .set = "smoke"
        end | del(.tags)' all.json |
      while read -r JOB_RUN
      do
        echo "$JOB_RUN" > input_without_hash.json
        URL=$(jq -r ._id input_without_hash.json)
        DATE=$(jq -r .started input_without_hash.json)
        mongoexport --uri "$MONGO_HOST" -c Results --query \
          '{"_id": "'"$URL"'", "started": "'"$DATE"'"}' > existing.json

        # Fetch hash if not already in database
        if [ -s existing.json ]
        then
          HASH=$(jq -r .hash existing.json)
        else
          curl -m1200 -gsSf "$URL/consoleText" > console
          HASH=$(grep "\[INFO\] : The used hash is " console | grep -o "[^ ]*$")
        fi
        if [ -z "$HASH" ]
        then
          cp input_without_hash.json input_with_hash.json
        else
          jq '.hash = "'"$HASH"'"[:8]' input_without_hash.json > input_with_hash.json
        fi

        # Fetch test cases if not already in database
        if [ -s existing.json ] && jq -e .tests existing.json > /dev/null
        then
          TESTS=$(jq .tests existing.json)
        else
          TESTS=$(curl -m1200 -gsSf "$URL/robot/report/output.xml" | parse_test_cases.py)
        fi
        if [ -z "$TESTS" ]
        then
          TESTS=[]
        fi
        jq ".tests = $TESTS" input_with_hash.json > input_with_tests.json

        mongoimport --uri "$MONGO_HOST" -c Results --mode upsert \
          --file input_with_tests.json
      done
    fi
  fi
  echo "Sleep until new run"
  sleep "$REFRESH_INTERVAL"
done
